const Event = require('../../models/event')
const User = require('../../models/user')
const {dateToString} = require('../../helpers/date')
const Dataloader = require('dataloader')

const eventLoader = new Dataloader((eventIds) =>{
    console.log(eventIds)
    return events(eventIds)
})

const userLoader = new Dataloader((userIds)=>{
    console.log(userIds)
    return User.find({_id:{$in:userIds}})
})


const user = userId =>{
    return userLoader.load(userId.toString())
    .then(user=>{
        return {...user._doc, createdEvents: () => eventLoader.loadMany(user._doc.createdEvents)}
    })
    .catch(err =>{
        throw err
    })
}

const singleEvent = eventId =>{
    return eventLoader.load(eventId.toString())
    .then(event=>{
        return event
    })
    .catch(err =>{
        throw err
    })
}

const events = eventIds =>{
    return Event.find({_id:{$in:eventIds}})
    .then(events =>{
        return events.map(event =>{
            return transformEvent(event)
        })
    })
    .catch(err =>{
        throw err
    })
}

const transformEvent = event =>{
    return {
          ...event._doc,
            date:dateToString(event._doc.date),
            creator: user.bind(this,event._doc.creator)
    }
}

const transformBooking = booking =>{
    return{
        ...booking._doc,
        createdAt:dateToString(booking._doc.createdAt),
        updatedAt:dateToString(booking._doc.updatedAt),
        user: user.bind(this, booking._doc.user),
        event: singleEvent.bind(this, booking._doc.event)
    }
}


exports.transformEvent = transformEvent
exports.transformBooking = transformBooking