const Booking = require('../../models/booking')
const Event = require('../../models/event')
const {transformBooking} = require('./merge')




module.exports = {
        bookings:(req) =>{
            if(!req.isAuth) throw new Error("Unauthenticated")
            return Booking.find()
            .then(res =>{
                return res.map(booking =>{
                    return transformBooking(booking)
                })
            }).catch(err =>{
                throw err
            })
        },
        bookEvent:(args, req) =>{
            if(!req.isAuth) throw new Error("Unauthenticated")
           return Event.findById(args.eventId).then(event =>{
                if(!event){
                    throw new Error('Event does not exist')
                }
                const booking  = new Booking({
                    user:req.userId,
                    event:event.id
                })
                return booking.save()
            })
            .then(result =>{
                return transformBooking(result)
            })
            .catch(err =>{
                throw err
            })
        },

        cancelBooking:async (args,req) =>{
            if(!req.isAuth) throw new Error("Unauthenticated")
            try{
                const booking = await Booking.findById(args.bookingId).populate('event')
                const event = transformEvent(booking.event)
                await Booking.deleteOne({_id:args.bookingId})
                return event

            }catch(err){
                throw err
            }

        }
    }