const express = require('express')
const mongoose = require('mongoose')
const cors = require('cors')
const { graphqlHTTP } = require('express-graphql');
const  schema = require('./graphql/schema/schema')
const resolvers = require('./graphql/resolvers/resolvers')
const isAuth = require('./middleware/is-auth')
require('dotenv/config')

const app = express()


//MIDDLEWARE
app.use(express.json())
app.use(express.urlencoded({
    extended:true
}))
app.use(cors())

app.use(isAuth)

app.use('/graphql', graphqlHTTP({
    schema: schema,
    rootValue: resolvers,
    graphiql: true
}))


mongoose.connect(process.env.DB_CONNECTION,{ useNewUrlParser: true, useFindAndModify:true, useUnifiedTopology:true }, () => console.log("connected to DB"))
app.listen(process.env.PORT ||5000, () => console.log("Server is up"))
